﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Accelist.Example.Docker.Services;
using Accelist.Example.Docker.Services.Models.Articles;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Accelist.Example.Docker.Web.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ArticleService ArticleService;

        public IndexModel(ArticleService articleService)
        {
            this.ArticleService = articleService;
        }

        public List<ArticleModel> Articles { get; set; }

        public async Task OnGetAsync()
        {
            this.Articles = await this.ArticleService.GetArticles();
        }
    }
}
