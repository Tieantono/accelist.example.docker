﻿using System;
using System.Threading.Tasks;
using Accelist.Example.Docker.Services;
using Accelist.Example.Docker.Services.Models.Articles;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Accelist.Example.Docker.Web.Pages.Article
{
    public class IndexModel : PageModel
    {
        private readonly ArticleService ArticleService;

        public IndexModel(ArticleService articleService)
        {
            this.ArticleService = articleService;
        }

        public ArticleModel Article { get; set; }

        public async Task OnGetAsync(Guid articleId)
        {
            this.Article = await this.ArticleService.GetArticle(articleId);
        }
    }
}