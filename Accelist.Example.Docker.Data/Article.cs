﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accelist.Example.Docker.Data
{
    public partial class Article
    {
        public Guid ArticleId { get; set; }
        [Required]
        [StringLength(128)]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
        [Column(TypeName = "timestamp with time zone")]
        public DateTime CreatedAt { get; set; }
    }
}
