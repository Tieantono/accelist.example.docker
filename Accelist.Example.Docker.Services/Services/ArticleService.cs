﻿using Accelist.Example.Docker.Data;
using Accelist.Example.Docker.Services.Models.Articles;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.Example.Docker.Services
{
    public class ArticleService
    {
        private readonly ArticleDbContext DB;

        public ArticleService(ArticleDbContext db)
        {
            this.DB = db;
        }

        public async Task<List<ArticleModel>> GetArticles(Guid? articleId = null)
        {
            var articleQuery = this.DB
                .Article
                .Select(Q => new ArticleModel
                {
                    ArticleId = Q.ArticleId,
                    Title = Q.Title,
                    Content = Q.Content
                });

            if(articleId.HasValue == true)
            {
                articleQuery.Where(Q => Q.ArticleId == articleId);
            }

            var articles = await articleQuery.ToListAsync();

            return articles;
        }

        public async Task<ArticleModel> GetArticle(Guid articleId)
        {
            var articles = await this.DB
                .Article
                .Where(Q => Q.ArticleId == articleId)
                .Select(Q => new ArticleModel
                {
                    ArticleId = Q.ArticleId,
                    Title = Q.Title,
                    Content = Q.Content
                })
                .FirstOrDefaultAsync();

            return articles;
        }
    }
}
