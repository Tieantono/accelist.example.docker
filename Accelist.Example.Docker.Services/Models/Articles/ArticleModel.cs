﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Accelist.Example.Docker.Services.Models.Articles
{
    public class ArticleModel
    {
        public Guid ArticleId { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }
    }
}
