# Accelist.Example.Docker

An example project that consists multiple applications in a Docker container environment.

To build a Docker image, you must have a Dockerfile in your project.
Command:
`docker build -t myapp .` (replace myapp with your desired name)

To run the Docker image in a Docker container, use this following command:
`docker run -d -p 8080:80 --name myapp myapp`